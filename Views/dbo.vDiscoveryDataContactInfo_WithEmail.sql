SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





  /****** Script for SelectTopNRows command from SSMS  ******/
  CREATE view [dbo].[vDiscoveryDataContactInfo_WithEmail] as
with cte_Discovery_Email_Prep as (
SELECT case when Branch_PhoneDoNotCall != 'DNC'
		THEN [Branch_Phone]
		else Null
		end as [Discovery Branch Phone]
      ,case when [Email_BusinessType] != ''
	  then [Email_BusinessType]
	  end as [Discovery Email]
	  ,case 
	  when [Email_Business2Type] != ''
	  then [Email_Business2Type]
	  end as [Discovery Secondary Email]
	  ,case when [Email_PersonalType] != ''
	  then [Email_PersonalType]
	  else null
	  end as [Discovery Personal Email]
	  ,[PersonalWebpage]
      ,[FirmWebsite]
      ,[SocialMedia_LinkedIn]
	  ,sf.email
	  ,sf.phone
	  
      ,[RepCRD]
	  ,sf.id
  FROM [AdHoc].[dbo].[DiscoveryData-6-11-2021] c
  join openquery(SALESFORCEDEVART, 'select id, CRD__c, Email, Phone, Broker_Dealer__c from Contact where CRD__c != null') sf on sf.CRD__c = c.RepCRD
--  join openquery(SALESFORCEDEVARTFULLCOPY, 'select id, Firm_CRD__c from Broker_Dealer__c where Firm_CRD__c != null') sf_firm on sf.Broker_Dealer__c = sf_Firm.id
--  where sf_firm.Firm_CRD__c in ('165868',
--'26898')
  )

  select id,
  case when [Discovery Branch Phone] is null
  then ''
  else [Discovery Branch Phone]
  end as [Discovery Branch Phone],
  case when [Discovery Email] is null
  then ''
  else [Discovery Email]
  end as [Discovery Email],
  case when [Discovery Secondary Email] is null
  then ''
  else [Discovery Secondary Email]
  end as [Discovery Secondary Email],
  case when [Discovery Personal Email] is null
  then '' 
  else [Discovery Personal Email]
  end as [Discovery Personal Email],
  case when email is not null
	then email
	when email is null and [Discovery Email] is not null
    then [discovery email]
	when email is null and [Discovery Email]  is null and [Discovery Secondary Email] is not null
	then [Discovery secondary Email]
	else ISNULL(email, '')
	end as email,
  case when phone is null
  then [Discovery Branch Phone]
  else phone
  end as phone,
  case when [PersonalWebpage] is null
  then ''
  else [PersonalWebpage]
  end as [Discovery Personal Web Page]
  ,case when [FirmWebsite] is null
  then ''
  else [FirmWebsite] 
  end as [Discovery Firm Website]
  ,case when [SocialMedia_LinkedIn] is null
  then ''
  else [SocialMedia_LinkedIn] 
  end as [Discovery LinkedIn Page]

  From cte_Discovery_Email_Prep


GO
