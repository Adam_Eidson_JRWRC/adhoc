SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vCompetitorOfferingPArticipation_FirmInfo] as
with cte_FormD_Participation as (
  Select distinct --EntityName, 
--f.FILING_DATE,
--i.ACCESSIONNUMBER,
--RECIPIENT_SEQ_KEY,
--RECIPIENTNAME,
RECIPIENTCRDNUMBER	,
count(*) as total
----ASSOCIATEDBDNAME,
--ASSOCIATEDBDCRDNUMBER,
--INDUSTRYGROUPTYPE,
--MINIMUMINVESTMENTACCEPTED,
--TOTALAMOUNTSOLD
From RegulatoryAgency.SEC.ISSUERS i
join RegulatoryAgency.sec.OFFERING o on o.ACCESSIONNUMBER = i.ACCESSIONNUMBER
join RegulatoryAgency.SEC.RECIPIENTS r on r.ACCESSIONNUMBER = o.ACCESSIONNUMBER
join RegulatoryAgency.SEC.FORMDSUBMISSION f on f.ACCESSIONNUMBER = o.ACCESSIONNUMBER
join AdHoc.dbo.CompetitorOffering co on co.InvestmentName = i.ENTITYNAME

group by RECIPIENTCRDNUMBER--, EntityName

union
Select distinct --EntityName, 
--f.FILING_DATE,
--i.ACCESSIONNUMBER,
--RECIPIENT_SEQ_KEY,
--RECIPIENTNAME,
--RECIPIENTCRDNUMBER,	
----ASSOCIATEDBDNAME,
ASSOCIATEDBDCRDNUMBER,
count(*) as total
--INDUSTRYGROUPTYPE,
--MINIMUMINVESTMENTACCEPTED,
--TOTALAMOUNTSOLD
From RegulatoryAgency.SEC.ISSUERS i
join RegulatoryAgency.sec.OFFERING o on o.ACCESSIONNUMBER = i.ACCESSIONNUMBER
join RegulatoryAgency.SEC.RECIPIENTS r on r.ACCESSIONNUMBER = o.ACCESSIONNUMBER
join RegulatoryAgency.SEC.FORMDSUBMISSION f on f.ACCESSIONNUMBER = o.ACCESSIONNUMBER
join AdHoc.dbo.CompetitorOffering co on co.InvestmentName = i.ENTITYNAME
group by ASSOCIATEDBDCRDNUMBER--, EntityName
)


select distinct f.FINRANumber, f.name,
'https://brokercheck.finra.org/firm/summary/'+cast(f.FINRANumber as varchar(15)) as [FINRA Firm URL],
f.FINRAStatus as Firm_FINRAStatus,
f.SECStatus as Firm_SECStatus,
'https://na57.salesforce.com/'+sf.id as [SF URL],
f.HighNetWorthAuM,
f.NumberOfHighNetWorthClients,
sum(cfp.total) as total
from cte_FormD_Participation cfp
join RegulatoryAgency.dbo.firm f on cast(f.FINRANumber as varchar(15)) = cast(cfp.RECIPIENTCRDNUMBER as varchar(15))
left join openquery(SALESFORCEDEVART, 'Select id, Firm_CRD__c from Broker_Dealer__c where Firm_CRD__c != ''''') sf on sf.Firm_CRD__c = f.FINRANumber
group by f.FINRANumber, f.name,
f.FINRANumber,
f.FINRAStatus,
f.SECStatus,
sf.id,
f.HighNetWorthAuM,
f.NumberOfHighNetWorthClients
GO
