SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vBlackCreekEntity_IndividualCRDAssociation] as
with cte_Discovery_Email_Prep as (
SELECT case when Branch_PhoneDoNotCall != 'DNC'
		THEN [Branch_Phone]
		else Null
		end as [Discovery Branch Phone]
      ,case when [Email_BusinessType] != ''
	  then [Email_BusinessType]
	  end as [Discovery Email]
	  ,case 
	  when [Email_Business2Type] != ''
	  then [Email_Business2Type]
	  end as [Discovery Secondary Email]
	  ,case when [Email_PersonalType] != ''
	  then [Email_PersonalType]
	  else null
	  end as [Discovery Personal Email]
	  ,[PersonalWebpage]
      ,[FirmWebsite]
      ,[SocialMedia_LinkedIn]
	  ,sf.email
	  ,sf.phone
	  
      ,[RepCRD]
	  ,sf.id
  FROM [AdHoc].[dbo].[BlackCreek BD and RIA CRD Filter-ContactInfo] c
  left join openquery(SALESFORCEDEVART, 'select id, CRD__c, Email, Phone, Broker_Dealer__c from Contact where CRD__c != null') sf on sf.CRD__c = c.RepCRD
  --join openquery(SALESFORCEDEVARTFULLCOPY, 'select id, Firm_CRD__c from Broker_Dealer__c where Firm_CRD__c != null') sf_firm on sf.Broker_Dealer__c = sf_Firm.id
  --where sf_firm.Firm_CRD__c not in ('291902')
--'26898')

  ), cte_FormD_Participation as (
  Select distinct EntityName, 
f.FILING_DATE,
--i.ACCESSIONNUMBER,
--RECIPIENT_SEQ_KEY,
--RECIPIENTNAME,
RECIPIENTCRDNUMBER	
----ASSOCIATEDBDNAME,
--ASSOCIATEDBDCRDNUMBER,
--INDUSTRYGROUPTYPE,
--MINIMUMINVESTMENTACCEPTED,
--TOTALAMOUNTSOLD
From RegulatoryAgency.SEC.ISSUERS i
join RegulatoryAgency.sec.OFFERING o on o.ACCESSIONNUMBER = i.ACCESSIONNUMBER
join RegulatoryAgency.SEC.RECIPIENTS r on r.ACCESSIONNUMBER = o.ACCESSIONNUMBER
join RegulatoryAgency.SEC.FORMDSUBMISSION f on f.ACCESSIONNUMBER = o.ACCESSIONNUMBER
where i.ENTITYNAME in ('BC Exchange Stafford Grove DST',
'BC Exchange Salt Pond DST',
'BC Exchange Suniland DST',
'BC Exchange Northgate DST',
'DCX Springdale DST',
'BC Exchange Vasco DST'
)
union
Select distinct EntityName, 
f.FILING_DATE,
--i.ACCESSIONNUMBER,
--RECIPIENT_SEQ_KEY,
--RECIPIENTNAME,
--RECIPIENTCRDNUMBER,	
----ASSOCIATEDBDNAME,
ASSOCIATEDBDCRDNUMBER
--INDUSTRYGROUPTYPE,
--MINIMUMINVESTMENTACCEPTED,
--TOTALAMOUNTSOLD
From RegulatoryAgency.SEC.ISSUERS i
join RegulatoryAgency.sec.OFFERING o on o.ACCESSIONNUMBER = i.ACCESSIONNUMBER
join RegulatoryAgency.SEC.RECIPIENTS r on r.ACCESSIONNUMBER = o.ACCESSIONNUMBER
join RegulatoryAgency.SEC.FORMDSUBMISSION f on f.ACCESSIONNUMBER = o.ACCESSIONNUMBER
where i.ENTITYNAME in ('BC Exchange Stafford Grove DST',
'BC Exchange Salt Pond DST',
'BC Exchange Suniland DST',
'BC Exchange Northgate DST',
'DCX Springdale DST',
'BC Exchange Vasco DST'
)
)
select cfp.*
from cte_Discovery_Email_Prep cep
join cte_FormD_Participation cfp on cast(cfp.RECIPIENTCRDNUMBER as varchar(15))= cast(cep.RepCRD as varchar(15))


--BC Exchange Stafford Grove DST
--BC Exchange Salt Pond DST
--BC Exchange Suniland DST
--BC Exchange Perimeter DST
--BC Exchange Northgate DST
--DCX Springdale DST
--BC Exchange Vasco DST
GO
