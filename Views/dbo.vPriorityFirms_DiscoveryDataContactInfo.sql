SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






  /****** Script for SelectTopNRows command from SSMS  ******/
  CREATE view [dbo].[vPriorityFirms_DiscoveryDataContactInfo] as
with cte_Discovery_Email_Prep as (
SELECT case when Branch_PhoneDoNotCall != 'DNC'
		THEN [Branch_Phone]
		else Null
		end as [Discovery Branch Phone]
      ,case when [Email_BusinessType] != ''
	  then [Email_BusinessType]
	  end as [Discovery Email]
	  ,case 
	  when [Email_Business2Type] != ''
	  then [Email_Business2Type]
	  end as [Discovery Secondary Email]
	  ,case when [Email_PersonalType] != ''
	  then [Email_PersonalType]
	  else null
	  end as [Discovery Personal Email]
	  ,[PersonalWebpage]
      ,[FirmWebsite]
      ,[SocialMedia_LinkedIn]
	  ,sf.email
	  ,sf.phone
	  
      ,[RepCRD]
	  ,sf.id
  FROM [AdHoc].[dbo].[Priority Firm Contact Info] c
  join openquery(SALESFORCEDEVART, 'select id, CRD__c, Email, Phone, Broker_Dealer__c from Contact where CRD__c != null') sf on sf.CRD__c = c.RepCRD
  --join openquery(SALESFORCEDEVARTFULLCOPY, 'select id, Firm_CRD__c from Broker_Dealer__c where Firm_CRD__c != null') sf_firm on sf.Broker_Dealer__c = sf_Firm.id
  --where sf_firm.Firm_CRD__c  in ('151604')
--'26898')
  )

  select id,
  ISNULL([Discovery Branch Phone],'') as [Discovery Branch Phone],
  ISNULL([Discovery Email],'') as [Discovery Email],
  ISNULL([Discovery Secondary Email],'') as [Discovery Secondary Email],
  ISNULL([Discovery Personal Email],'') as [Discovery Personal Email],
  ISNULL([PersonalWebpage],'') as [Discovery Personal Web Page]
  ,ISNULL([FirmWebsite],'') as [Discovery Firm Website]
  ,ISNULL([SocialMedia_LinkedIn] ,'') as [Discovery LinkedIn Page]
  ,[RepCRD]
  From cte_Discovery_Email_Prep


GO
