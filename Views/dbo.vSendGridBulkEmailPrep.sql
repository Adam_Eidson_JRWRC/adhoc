SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[vSendGridBulkEmailPrep] as 
with cte_processed as (
select email,
'Normal' as Priority,
    'Completed' as Status,
    'Email' as Type,
	cast(0 as bit) as IsReminderSet,
	TRIM(SUBSTRING(unique_args,CHARINDEX('"campaign_id":"',unique_args)+LEN('"campaign_id":"'),CHARINDEX('","', unique_args)-(CHARINDEX('"campaign_id":"',unique_args)+LEN('"campaign_id":"')))) as campaign_id,
	subject, 
	[from] as from_email_Address,
	convert(varchar(50),processed,126) as ActivityDate
from [AdHoc].[dbo].[Sendgrid Activity 12-20-2020 to 1-6-2021] base
where event = 'processed'
and unique_args like '%campaign_id%'
group by email, 
processed,
  subject,[from],
  unique_args
),cte_delivered as (
select email,
	cast('True' as bit) as Delivered,
	convert(varchar(50),processed,126) as Delivery_Date__c

from [AdHoc].[dbo].[Sendgrid Activity 12-20-2020 to 1-6-2021] base
where event = 'delivered'
and unique_args like '%campaign_id%'
group by email, 
  subject
  ,processed
),  cte_opened as (
SELECT 
      [event]
      ,[email]
	  ,count(*) as Opens

  FROM [AdHoc].[dbo].[Sendgrid Activity 12-20-2020 to 1-6-2021]
  where event = 'open'
  and unique_args like '%campaign_id%'
  group by email,
  [event], 
  subject
  ), cte_clicked as (
  SELECT 
      [event]
      ,[email]
	  ,count(*) as Clicks

  FROM [AdHoc].[dbo].[Sendgrid Activity 12-20-2020 to 1-6-2021]
  where event = 'click'
  and unique_args like '%campaign_id%'
  group by email,
  [event], 
  subject
  ), cte_firstOpen as (
  SELECT 
      [event]
      ,[email]
	  ,convert(varchar(50),min(processed),126) as FirstOpen

  FROM [AdHoc].[dbo].[Sendgrid Activity 12-20-2020 to 1-6-2021]
  where event = 'open'
  and unique_args like '%campaign_id%'
  group by email,
  [event], 
  subject
  ), cte_LastOpen as (
  SELECT 
      [event]
      ,[email]
	  ,convert(varchar(50),max(processed),126) as LastOpen

  FROM [AdHoc].[dbo].[Sendgrid Activity 12-20-2020 to 1-6-2021]
  where event = 'open'
  and unique_args like '%campaign_id%'
  group by email,
  [event]
  ), cte_firstClick as (
  SELECT 
      [event]
      ,[email]
	  ,convert(varchar(50),min(processed),126) as FirstClick

  FROM [AdHoc].[dbo].[Sendgrid Activity 12-20-2020 to 1-6-2021]
  where event = 'click'
  and unique_args like '%campaign_id%'
  group by email,
  [event], 
  subject
  ), cte_LastClick as (
  SELECT 
      [event]
      ,[email]
	  ,convert(varchar(50),max(processed),126) as LastClick

  FROM [AdHoc].[dbo].[Sendgrid Activity 12-20-2020 to 1-6-2021]
  where event = 'click'
  and unique_args like '%campaign_id%'
  group by email,
  [event], 
  subject
  ), cte_Complaint as (
  SELECT 
      cast(1 as bit) as Complaint
	  , cast(1 as bit) as Unsubscribed
      ,[email]
	 

  FROM [AdHoc].[dbo].[Sendgrid Activity 12-20-2020 to 1-6-2021]
  where event = 'spamreport'
  and unique_args like '%campaign_id%'
  group by email, 
  subject
  ), cte_Bounce as (
  SELECT 
      cast(1 as bit) as Bounce
      ,[email]
	 

  FROM [AdHoc].[dbo].[Sendgrid Activity 12-20-2020 to 1-6-2021]
  where event = 'bounce'
  and unique_args like '%campaign_id%'
  group by email, 
  subject
  )
  
  select distinct
  cp.ActivityDate,
  cp.email as To_Address__c,
cp.Priority,
    cp.Status,
    cp.Type,
	cp.IsReminderSet,
	campaign_id as Campaign_Id__c,
	subject, 
	from_email_Address,
  case when cd.Delivered is null
  then cast(0 as bit)
  else cast(cd.Delivered as bit)
  end as Delivered__c,
  case when cd.Delivery_Date__c is null
  then ''
  else ActivityDate
  end as Delivery_Date__c,
  case when co.Opens is null
		then 0 
		else co.Opens
		end as Opens__c,
  case when cfo.FirstOpen is null
  then ''
  else cfo.FirstOpen 
  end as First_Open_Date__c,
  case when clo.LastOpen is null
  then ''
  else clo.LastOpen 
  end as Last_Open_Date__c,
  case when cc.Clicks is null
		then 0
		else cc.Clicks
		end as Clicks__c,
  case when cfc.FirstClick is null
  then ''
  else cfc.FirstClick
  end as First_Click_Date__c,
  case when clc.LastClick is null
		then ''
		else clc.LastClick
		end as Last_Click_Date__c,
  case when ccomp.Complaint is null 
		then cast(0 as bit)
		else ccomp.Complaint
		end as Complained__c,
  case when ccomp.Unsubscribed is null
  then cast(0 as bit)
  else ccomp.Unsubscribed
  end as Unsubscribed__c,
  case when cb.Bounce is null
  then cast(0 as bit)
  else cb.Bounce
  end as Bounced__c
  From cte_processed cp
  left join cte_delivered cd on cd.email = cp.email
  left join cte_opened co on cd.email = co.email
  left join cte_clicked cc on cc.email = co.email
  left join cte_firstOpen cfo on cfo.email = cd.email
  left join cte_lastOpen clo on clo.email = cd.email
  left join cte_firstClick cfc on cfc.email = cd.email
  left join cte_LastClick clc on clc.email = cd.email
  left join cte_Complaint ccomp on ccomp.email = cd.email
  left join cte_Bounce cb on cb.email = cp.email
 where ActivityDate  > '2020-12-19'
and subject in ('ACTION REQUIRED: Net-Leased Portfolio 42 DST Now Accepting Funds',
'TCF12 - Sale Letter',
'TCF2 Stella Mare Sale Update')
GO
