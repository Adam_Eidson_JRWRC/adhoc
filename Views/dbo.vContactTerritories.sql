SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--/****** Script for SelectTopNRows command from SSMS  ******/
--SELECT TOP (1000) [IndividualCRD]
--      ,[State]
--      ,[City]
--      ,[Street1]
--      ,[Street2]
--      ,[latitude]
--      ,[longitude]
--      ,[County]
--  FROM [AdHoc].[dbo].[ContactLocation]'CA'' Kern''Kern''CA'''
CREATE view [dbo].[vContactTerritories] as
  with cte_Territories as (
  select *
  From openquery(SALESFORCEDEVART,'select  States__c, Sales_Region__c, Counties__c, User_Id__c from ER_Sales_Territory_Setting__mdt') 
  ), cte_Users as (
  select *
  From openquery(SALESFORCEDEVART,'select Id, Alias from User')

  ), cte_PreppedTerritories as (
  select *
  From cte_Territories ct
  join cte_Users cu on cu.Id like ct.User_Id__c+'%'
  ), cte_PreppedSplitTerritories as (
  select cpt.*,
  stateSplit.value as [state],
  CountySplit.value as county
  from cte_PreppedTerritories cpt
  cross apply string_split(cpt.States__c, ',') as stateSplit
  cross apply string_split(isnull(cpt.Counties__c,''),',') as CountySplit
  )
  Select distinct cpt.alias,
  cpt.[Sales_Region__c],
  cpt.states__c,
  cpt.counties__c,
  cl.*
  From cte_PreppedSplitTerritories cpt
  join [AdHoc].[dbo].[ContactLocation] cl on Trim(cpt.state) = cl.state
  and trim(cpt.county) = Case when cpt.county = ''
  then ''
  else cl.County
  end
								
GO
