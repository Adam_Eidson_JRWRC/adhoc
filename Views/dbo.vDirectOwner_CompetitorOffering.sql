SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [dbo].[vDirectOwner_CompetitorOffering] as
SELECT distinct [IndividualCRD]
,i.FirstName
,i.lastName
,case when i.FINRAStatus = 'active'
  and i.SECStatus = 'Active'
  Then 'Dually Registered'
  when i.FINRAStatus = 'Active'
  and i.SECStatus != 'Active'
  then 'Broker-Dealer'
  when i.FINRAStatus != 'Active'
  and i.SECStatus = 'Active' 
  then 'RIA'
  else 'Unknown'
  end as [Record Type]
  ,fdo.name as [FINRA/SEC Current Firm Name]
	  ,sf.phone as [Salesforce Work Phone]
	  ,sf.MobilePhone as [Salesforce Cell Phone]
	  ,sf.OtherPhone as [Salesforce Alternate Phone]
	  ,case when cci.Branch_PhoneDoNotCall != 'DNC'
		THEN [Branch_Phone]
		else Null
		end as [Discovery Branch Phone]
	  ,sf.email as [Salesforce Email]
	  ,sf.Other_Email_Addresses__c as [Salesforce Alternate Emails]
	  , ISNULL([Email_BusinessType],'') as [Discovery Email]
	  ,ISNULL([Email_Business2Type], '') as [Discovery Secondary Email]
	  ,ISNULL([Email_PersonalType],'') as [Discovery Personal Email]
	  ,f.NumberOfHighNetWorthClients as [FINRA/SEC Firm Number of High Net-Worth Clients]
	  ,f.HighNetWorthAuM as [FINRA/SEC Firm High Net-Worth AUM]
	  ,vif.StartDate as [FINRA/SEC Current Firm Start Date]
	  ,fdo.FINRANumber as [FINRA/SEC Current Firm CRD]
	  ,[FirmWebsite] as [Discovery Firm Website URL]
	  ,[SocialMedia_LinkedIn] as [Discovery LinkedIn Page URL]
	  ,[PersonalWebpage] as [Discovery Personal Web Page URL]
      ,'https://na57.salesforce.com/'+sf.id as [Salesforce URL]
      ,'https://brokercheck.finra.org/individual/summary/'+cast(fdo.individualCRD as varchar(15)) as [FINRA Summary URL]
      ,[IndividualRole]
	  --,cci.*
  FROM [AdHoc].[dbo].[vCompetitorOfferingPArticipation_FirmDirectOwner] fdo
   join RegulatoryAgency.dbo.Individual i on i.CRDNumber = fdo.individualCRD
  left join RegulatoryAgency.dbo.vIndividualFirm vif on vif.individualCRDNumber = i.CRDNumber
													and vif.EndDate is null
  left join RegulatoryAgency.dbo.firm f on f.FINRANumber = vif.firmCRDNumber
  left join [AdHoc].[dbo].[Form D Firms DirectOwner Combined-ContactInfoTemplate] cci on cci.RepCRD = fdo.IndividualCRD
  left join openquery(SALESFORCEDEVART, 'select id, CRD__c, Email, Phone, MobilePhone, OtherPhone, Other_Email_Addresses__c, Broker_Dealer__c from Contact where CRD__c != null') sf on sf.CRD__c = fdo.individualCRD

GO
