CREATE TABLE [dbo].[REITOfferings]
(
[Sponsor Name  Sponsor Name] [varchar] (49) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Investment Name] [varchar] (93) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type of Ownership] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Selling Agreement] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Form D] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CIK #] [varchar] (48) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Investment Category] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Asset Class] [varchar] (152) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
