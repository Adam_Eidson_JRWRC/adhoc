CREATE TABLE [dbo].[210528-CoStar-SellerProperties-ToPullDemos]
(
[Property ID] [varchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Property Name] [varchar] (96) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street] [varchar] (48) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Zip] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Longitude] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
