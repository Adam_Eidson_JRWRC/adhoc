CREATE TABLE [dbo].[Missing CRDs - Sheet3]
(
[ Project Status] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Contact ID] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[First Name] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Last Name] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Contact Record Type] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CRD #] [bigint] NULL,
[Broker-Dealer RIA  Firm CRD #] [bigint] NULL,
[Broker-Dealer RIA  Record Type] [varchar] (19) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Broker-Dealer RIA  Broker-Dealer RIA Name] [varchar] (49) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Prior Firm Name] [varchar] (26) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Mailing City] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Mailing State Province] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
