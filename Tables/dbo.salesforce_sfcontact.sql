CREATE TABLE [dbo].[salesforce_sfcontact]
(
[broker_dealer] [float] NULL,
[crd] [bigint] NULL,
[do_not_call] [bit] NULL,
[do_not_email] [bit] NULL,
[do_not_mail] [bit] NULL,
[does_not_require_signed_selling_agreement] [bit] NULL,
[email] [varchar] (53) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[finra_status] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[first_name] [varchar] (44) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[has_opted_out_of_email] [bit] NULL,
[id] [bigint] NULL,
[last_name] [varchar] (84) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[middle_name] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[record_type] [float] NULL,
[roles] [varchar] (69) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[sales_rep_signed_agreements] [varchar] (1238) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[sf_broker_dealer_id] [varchar] (19) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[sf_id] [varchar] (19) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[sf_record_type_id] [varchar] (19) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[sf_user_created_id] [varchar] (19) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[sf_user_created_name] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[status] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[suffix] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
