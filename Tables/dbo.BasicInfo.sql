CREATE TABLE [dbo].[BasicInfo]
(
[CRDNumber] [int] NULL,
[firstName] [varchar] (26) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[lastName] [varchar] (26) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[middleName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DaysInIndustryBeginDate] [datetime] NULL,
[bcScope] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[iaScope] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
