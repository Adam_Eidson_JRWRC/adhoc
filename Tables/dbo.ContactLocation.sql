CREATE TABLE [dbo].[ContactLocation]
(
[IndividualCRD] [int] NULL,
[State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street1] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Street2] [varchar] (33) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[latitude] [real] NULL,
[longitude] [real] NULL,
[County] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
