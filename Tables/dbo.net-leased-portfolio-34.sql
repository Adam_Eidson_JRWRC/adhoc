CREATE TABLE [dbo].[net-leased-portfolio-34]
(
[SF Contact ID] [varchar] (19) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (53) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[First Name] [varchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Last Name] [varchar] (22) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FINRA] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Does Not Req SDA] [bit] NULL,
[HasOptedOutOfEmail] [bit] NULL,
[Do Not Email] [bit] NULL,
[Do Not Mail] [bit] NULL,
[Do Not Call] [bit] NULL
) ON [PRIMARY]
GO
